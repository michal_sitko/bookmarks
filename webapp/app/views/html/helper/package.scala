package views.html.helper

import play.twirl.api.Html

/**
 * Created by michal on 18/12/14.
 */
package object dashgum {
  implicit val fieldConstructor = new FieldConstructor {
    override def apply(elts: FieldElements): Html =
      if(elts.label == null){
        views.html.common.dashgumInputWithoutLabel(elts)
      } else {
        views.html.common.dashgumInput(elts)
      }
  }
}
