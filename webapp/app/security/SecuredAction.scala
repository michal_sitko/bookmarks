package security

import controllers.routes
import play.api.mvc._

import concurrent.Future

case class SecuredAction[A](action: Action[A]) extends Action[A] {

  /**
   * EXERCISE 4.4 - SecuredAction
   *
   * Your task here is to implement apply method which execute wrapped action if user is logged in
   * Otherwise it redirects to `routes.UserController.login`
   *
   * Tip: you can access wrapped action as `action`
   */
  def apply(request: Request[A]): Future[Result] = {
    request.session.get("login") match {
      case Some(login) => action(request)
      case None => Future.successful(Results.Redirect(routes.UserController.login))
    }
  }

  // it enables different formats of requests, you don't have to worry about this
  override def parser: BodyParser[A] = action.parser
}