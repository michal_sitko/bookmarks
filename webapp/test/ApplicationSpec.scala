import java.util.concurrent.TimeUnit
import java.util.regex.Pattern

import org.scalatest.time.Millisecond
import org.scalatestplus.play.{PlaySpec, OneServerPerTest, OneAppPerTest}
import org.specs2.mutable._
import org.specs2.runner._
import org.junit.runner._
import play.api.mvc.Cookie

import play.api.test._
import play.api.test.Helpers._

import concurrent.Await
import concurrent.duration.Duration

@RunWith(classOf[JUnitRunner])
class ApplicationSpec extends PlaySpec with OneAppPerTest {

  "Registration form" should {
    "redirect to /register for incorrect data" in {
      val correctLogin = "someLogin"
      val correctPassword = "somePassword"
      val form = List(("login", correctLogin), ("email", "incorrectEmail"), ("password" -> correctPassword))
      val result = route(FakeRequest(POST, "/register").withFormUrlEncodedBody(form: _*)).get

      status(result) must equal(OK)
      contentAsString(result).contains("Valid email required") mustBe(true)
    }

    "redirect to /login for correct data" in {
      val correctLogin = "someLogin"
      val correctPassword = "somePassword"
      val form = List(("login", correctLogin), ("email", "someEmail@example.com"), ("password" -> correctPassword))
      val result = route(FakeRequest(POST, "/register").withFormUrlEncodedBody(form: _*)).get

      status(result) must equal(SEE_OTHER)
      headers(result).get("Location") must equal(Some("/login"))
    }
  }

  "Registered user" should {
    "be able to log in" in {
      val correctLogin = "someLogin2"
      val correctPassword = "somePassword"
      val form = List(("login", correctLogin), ("email", "someEmail2@example.com"), ("password" -> correctPassword))
      val result = route(FakeRequest(POST, "/register").withFormUrlEncodedBody(form: _*)).get

      status(result) must equal(SEE_OTHER)
      headers(result).get("Location") must equal(Some("/login"))

      val loginForm = List(("login", correctLogin), ("email", "someEmail"), ("password" -> correctPassword))
      val correctLoginResult = route(FakeRequest(POST, "/login").withFormUrlEncodedBody(loginForm: _*)).get

      status(correctLoginResult) must equal(SEE_OTHER)
      headers(correctLoginResult).get("Location") must equal(Some("/dashboard"))

      val incorrectLoginForm = List(("login", correctLogin), ("email", "someEmail"), ("password" -> "incorrectPassword"))
      val incorrectLoginResult = route(FakeRequest(POST, "/login").withFormUrlEncodedBody(incorrectLoginForm: _*)).get

      status(incorrectLoginResult) must equal(SEE_OTHER)
      headers(incorrectLoginResult).get("Location") must equal(Some("/login"))
    }
  }

  "Dashboard page" should {
    "be accessible for logged in user" in {
      val correctLogin = "someLogin3"
      val correctPassword = "somePassword"
      val form = List(("login", correctLogin), ("email", "someEmail3@example.com"), ("password" -> correctPassword))
      val result = route(FakeRequest(POST, "/register").withFormUrlEncodedBody(form: _*)).get

      status(result) must equal(SEE_OTHER)
      headers(result).get("Location") must equal(Some("/login"))

      val loginForm = List(("login", correctLogin), ("email", "someEmail"), ("password" -> correctPassword))
      val correctLoginResult = route(FakeRequest(POST, "/login").withFormUrlEncodedBody(loginForm: _*)).get

      status(correctLoginResult) must equal(SEE_OTHER)
      headers(correctLoginResult).get("Location") must equal(Some("/dashboard"))

      val responseWithAuthCookies = Await.result(correctLoginResult, Duration(1000, TimeUnit.MILLISECONDS))
      responseWithAuthCookies.header.headers.get("Set-Cookie") match {
        case Some(cookiesString) =>
          cookiesString.split(Pattern.quote(";")).find(cookieString => cookieString.startsWith("PLAY_SESSION")) match {
            case Some(authCookie) =>
              val cookieValue = authCookie.substring(authCookie.indexOf("=") + 1).replaceAll(Pattern.quote("\""), "")
              val cookie = Cookie("PLAY_SESSION", cookieValue)
              val dashboard = route(FakeRequest(GET, "/dashboard").withCookies(cookie)).get
              status(dashboard) must equal(OK)
            case None =>
              fail("no auth cookie in response")
          }

        case None =>
          fail("no cookies in response")
      }
    }

    "not be accessible for anonymous user" in {
      val dashboard = route(FakeRequest(GET, "/dashboard")).get
      status(dashboard) must equal(SEE_OTHER)
      headers(dashboard).get("Location") must equal(Some("/login"))
    }
  }

}
