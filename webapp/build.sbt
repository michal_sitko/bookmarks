name := """webapp"""

version := "1.0-SNAPSHOT"

lazy val root = (project in file(".")).enablePlugins(PlayScala)

scalaVersion := "2.11.1"

resolvers += "clojars.org" at "http://clojars.org/repo"

libraryDependencies ++= Seq(
  cache,
  ws,
  "org.scalatestplus"       %% "play"                % "1.2.0" % "test",
  "org.scalatest"           %% "scalatest"           % "2.2.1" % "test"
)
