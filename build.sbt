name := """bookmarks"""

version := "1.0-SNAPSHOT"

scalaVersion := "2.11.1"

lazy val root = project.in(file("."))
  .aggregate(core, webapp)

lazy val webapp = project
  .enablePlugins(PlayScala)
  .dependsOn(core)

lazy val core = project
