This is training material for Play training.
=================================

# Slides

Slides are available in `slides` directory, to run them you need to:

```
npm install
```

and then:

```
node plugin/notes-server
```

# Training application 

To run training application run:

```
./activator "project webapp" run
```

# Git tags

There are some git tags defined which may help you to follow the progress in the sequence of exercises. They may be not very readable now - it's TODO to make the clear.