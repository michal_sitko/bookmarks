package io.scalac.training.user

import org.scalatest.{BeforeAndAfter, Matchers, WordSpec}

class InMemoryUserDaoSpec extends WordSpec with Matchers {

  "saving user" should {
    "fail when user's login already exists" in new SingleRowContext {
      val newUser = oldUser.copy(login = "newLogin")
      userDao.insertUser(newUser) shouldEqual Left(PersistenceConstraintsViolated)
    }

    "fail when user's email already exists" in new SingleRowContext {
      val newUser = oldUser.copy(email = "newEmail")
      userDao.insertUser(newUser) shouldEqual Left(PersistenceConstraintsViolated)
    }

    "fail when primary id overflowed" in {
      val userDao = new InMemoryUserDao(1)
      val oldUser = NewUserHashed("someLogin", "someEmail", "password", false)
      userDao.insertUser(oldUser) shouldEqual Right(UserId("1"))

      val newUser = oldUser.copy(login = "newLogin", email = "newEmail")
      userDao.insertUser(newUser) shouldEqual Left(PrimaryIdOverflow)
    }

    "succeed when unique login and email passed" in new SingleRowContext {
      val newUser = oldUser.copy(login = "newLogin", email = "newEmail")
      userDao.insertUser(newUser) shouldEqual Right(UserId("2"))
    }
  }

  "getting user" should {
    "return None if user has not been saved" in new Context {
      userDao.getUserById(UserId("1")) shouldEqual None
    }

    "return Some if user has been saved" in new SingleRowContext {
      userDao.getUserById(UserId("1")) shouldEqual Some(User(UserId("1"), oldUser.login, oldUser.email, oldUser.passwordHash, oldUser.hasPremiumAccount))
    }
  }

  "getByCriteria" should {
    "return all" in new ManyRowsContext {
      val users = userDao.getUsersByCriteria(UserCriteria())
      users.size shouldEqual 3
    }

    "return by login" in new ManyRowsContext {
      val users = userDao.getUsersByCriteria(UserCriteria(login = Some("someLogin")))
      users.size shouldEqual 1
    }

    "return by password" in new ManyRowsContext {
      val users = userDao.getUsersByCriteria(UserCriteria(passwordHash = Some("password2")))
      users.size shouldEqual 2
    }

    "return by login and password" in new ManyRowsContext {
      val users = userDao.getUsersByCriteria(UserCriteria(login = Some("someLogin3"), passwordHash = Some("password2")))
      users.size shouldEqual 1
    }
  }


  trait Context {
    val userDao = new InMemoryUserDao
  }

  trait SingleRowContext extends Context {
    val oldUser = NewUserHashed("someLogin", "someEmail", "password", false)
    userDao.insertUser(oldUser) shouldEqual Right(UserId("1"))
  }

  trait ManyRowsContext extends Context {
    val user1 = NewUserHashed("someLogin", "someEmail", "password", false)
    userDao.insertUser(user1) shouldEqual Right(UserId("1"))

    val user2 = NewUserHashed("someLogin2", "someEmail2", "password2", false)
    userDao.insertUser(user2) shouldEqual Right(UserId("2"))

    val user3 = NewUserHashed("someLogin3", "someEmail3", "password2", false)
    userDao.insertUser(user3) shouldEqual Right(UserId("3"))
  }

}
