package io.scalac.training.user

import io.scalac.training.crypto.Crypto
import org.scalamock.scalatest.MockFactory
import org.scalatest.{Matchers, WordSpec}

class UserServiceSpec
  extends WordSpec
  with Matchers
  with MockFactory {

  "insertUser" should {
    "return Some(User) if UserDao returned success" in new Context {
      val newUser = NewUser("someLogin", "someEmail", "somePassword", false)
      (cryptoMock.hash _) expects(*) returning "somePasswordHashed" once()
      val expectedUserId = UserId("someId")
      (userDaoMock.insertUser _) expects(*) returning Right(expectedUserId) once()
      userService.insertUser(newUser) shouldEqual Some(User(expectedUserId, "someLogin", "someEmail", "somePasswordHashed", false))
    }

    "return None if UserDao returned Failure" in new Context {
      val newUser = NewUser("someLogin", "someEmail", "somePassword", false)
      (cryptoMock.hash _) expects(*) returning "somePasswordHashed" once()
      (userDaoMock.insertUser _) expects(*) returning Left(PersistenceConstraintsViolated) once()

      userService.insertUser(newUser) shouldEqual None
    }
  }

  "validCredentials" should {
    "return true if user with criteria exists" in new Context {
      (userDaoMock.getUsersByCriteria _) expects(*) returning Set(User(UserId("userId"), "login", "email", "password", false))
      userService.validCredentials(UserCredentials("login", "password")) shouldEqual true
    }

    "return false if user with criteria does not exist" in new Context {
      (userDaoMock.getUsersByCriteria _) expects(*) returning Set.empty[User]
      userService.validCredentials(UserCredentials("login", "password")) shouldEqual false
    }
  }

  trait Context {
    val userDaoMock = mock[UserDao]
    val cryptoMock = mock[Crypto]
    val userService = new UserService(userDaoMock, cryptoMock)
  }

}
