package io.scalac.training.crypto

import com.roundeights.hasher.Algo

trait Crypto {
  def hash(input: String): String
}

object Crypto extends Crypto {
  def hash(input: String): String = {
    Algo.sha256(input).hash.toString
  }
}
