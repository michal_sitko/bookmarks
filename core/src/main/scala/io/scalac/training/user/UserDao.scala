package io.scalac.training.user

case class UserCriteria(login: Option[String] = None, passwordHash: Option[String] = None)

sealed trait PersistenceError
case object PersistenceConstraintsViolated extends PersistenceError
case object PrimaryIdOverflow extends PersistenceError


trait UserDao {
  def insertUser(newUser: NewUserHashed): Either[PersistenceError, UserId]
  def getUserById(userId: UserId): Option[User]
  def getUsersByCriteria(criteria: UserCriteria): Set[User]
}

class InMemoryUserDao(maxId: Int = Int.MaxValue) extends UserDao {
  private var insertedUsers = Map.empty[UserId, User]
  private var nextId = 1

  /**
   * Exercise 2.1
   *
   * Change return type to Either[PersistenceError, UserId] and implement this method to pass all tests in InMemoryUserDaoSpec.
   */
  override def insertUser(newUser: NewUserHashed): Either[PersistenceError, UserId] = {
    getViolation(newUser) match {
      case None =>
        val user = User(UserId(nextId.toString), newUser.login, newUser.email, newUser.passwordHash, false)
        nextId += 1
        insertedUsers += (user.id -> user)
        Right(user.id)
      case Some(violation) =>
        Left(violation)
    }
  }

  override def getUserById(userId: UserId): Option[User] = insertedUsers.get(userId)

  /**
   * Exercise 2.5 - ping pong pair programming - implementing InMemoryUserDao.getUsersByCriteria
   *
   * The semantics of this method should be following:
   * - for UserCriteria() should return all users
   * - for UserCriteria(login = Some(...)) should return all users with given login
   * - for UserCriteria(password = Some(...)) should return all users with given password
   * - for UserCriteria(login = Some(...), password = Some(...)) should return all users with given login AND password
   */
  def getUsersByCriteria(criteria: UserCriteria): Set[User] = {
    val predicate = prepareCriteriaPredicate(criteria)
    insertedUsers.values.filter(predicate).toSet
  }

  private def prepareCriteriaPredicate(criteria: UserCriteria): User => Boolean = {
    val defaultFun = (user: User) => true

    val loginPredicate = criteria.login match {
      case Some(login) =>
        (user: User) => user.login == login
      case None =>
        defaultFun
    }

    val passwordPredicate = criteria.passwordHash match {
      case Some(passwordHash) =>
        (user: User) => user.passwordHash == passwordHash
      case None =>
        defaultFun
    }

    val predicates = List(loginPredicate, passwordPredicate)
    predicates.foldLeft((user: User) => true){ (funUnderConstruction, predicate) =>
      (user: User) =>
        if(funUnderConstruction(user)) {
          predicate(user)
        } else {
          false
        }
    }
  }

  /**
   * Exercise 2.2 - implement getViolation and use it in insertUser
   *
   * Tip 1: use Option.orElse method
   *
   */
  private def getViolation(userToInsert: NewUserHashed): Option[PersistenceError] = {
    def getPrimaryIdOverflow: Option[PersistenceError] = {
      if(nextId < 0 || nextId > maxId) {
        Some(PrimaryIdOverflow)
      } else {
        None
      }
    }

    def getUniqueViolation: Option[PersistenceError] = {
      insertedUsers.values.find { user =>
        user.email == userToInsert.email || user.login == userToInsert.login
      } map { foundUser =>
        PersistenceConstraintsViolated
      }
    }

    getPrimaryIdOverflow orElse getUniqueViolation
  }

}
