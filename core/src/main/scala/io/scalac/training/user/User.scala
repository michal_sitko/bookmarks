package io.scalac.training.user

case class UserId(value: String)

case class NewUser(login: String, email: String, password: String, hasPremiumAccount: Boolean) {
  def createHashed(passwordToHash: String => String): NewUserHashed = {
    NewUserHashed(login, email, passwordToHash(password), hasPremiumAccount)
  }
}

case class NewUserHashed(login: String, email: String, passwordHash: String, hasPremiumAccount: Boolean)

case class User(id: UserId, login: String, email: String, passwordHash: String, hasPremiumAccount: Boolean)

case class UserCredentials(login: String, password: String)
