package io.scalac.training.user

import io.scalac.training.crypto.Crypto

class UserService (userDao: UserDao, crypto: Crypto) {
  /**
   * EXERCISE 3.3 - implement insertUser
   *
   * Should return Some(User) if userDao.insertUser was successful. Otherwise should return None
   */
  def insertUser(newUser: NewUser): Option[User] = {
    val hashedUser = newUser.createHashed(crypto.hash)
    val userIdEither = userDao.insertUser(hashedUser)
    userIdEither match {
      case Right(userId) =>
        val user = User(userId, hashedUser.login, hashedUser.email, hashedUser.passwordHash, hashedUser.hasPremiumAccount)
        Some(user)
      case Left(_) =>
        None
    }
  }

  /**
   * EXERCISE 3.4 - implement validCredentials
   */
  def validCredentials(userCredentials: UserCredentials): Boolean = {
    userDao.getUsersByCriteria(UserCriteria(Some(userCredentials.login), Some(userCredentials.password))).headOption.isDefined
  }

  def getUser(userId: UserId): Option[User] = userDao.getUserById(userId)
}
