name := """core"""

version := "1.0-SNAPSHOT"

scalaVersion := "2.11.1"

resolvers ++= Seq(
  "RoundEights" at "http://maven.spikemark.net/roundeights"
)

libraryDependencies ++= Seq(
  "com.roundeights"         %% "hasher"                        % "1.0.0",
  "org.scalamock"           %% "scalamock-scalatest-support"   % "3.2"    % "test",
  "org.scalatest"           %% "scalatest"                     % "2.2.1"  % "test"
)